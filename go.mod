module gitlab.com/FlmBus/copycat

go 1.22.1

require (
	github.com/goccy/go-yaml v1.11.3
	github.com/spf13/pflag v1.0.5
	github.com/tiagomelo/go-clipboard v0.1.0
)

require (
	github.com/fatih/color v1.16.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/xerrors v0.0.0-20231012003039-104605ab7028 // indirect
)
