package menu

import "gitlab.com/FlmBus/copycat/internal/snippets"

type Menu interface {
	GetSnippetGroups() []*snippets.Group
}
