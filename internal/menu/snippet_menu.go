package menu

import "gitlab.com/FlmBus/copycat/internal/snippets"

type SnippetMenu struct {
	Groups []*snippets.Group // categorized. hence the map
}

var _ Menu = &SnippetMenu{}

func (menu *SnippetMenu) AddGroup(group *snippets.Group) {
	menu.Groups = append(menu.Groups, group)
}

func (menu *SnippetMenu) GetSnippetGroups() []*snippets.Group {
	return menu.Groups
}
