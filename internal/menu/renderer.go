package menu

type MenuRenderer interface {
	Render(Menu) (ListItem, error)
}
