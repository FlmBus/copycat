package rofi

import (
	"fmt"

	"gitlab.com/FlmBus/copycat/internal/menu"
)

type TitleItem struct {
	Icon string
	Name string
}

var _ menu.ListItem = &TitleItem{}

func (item *TitleItem) String() string {
	return fmt.Sprintf("%-1s ", item.Icon)
	//return fmt.Sprintf("%s <span weight='bold' font-size='small'>%s</span>", item.Icon, item.Name)
}
