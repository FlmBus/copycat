package rofi

import (
	"bytes"
	"fmt"
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/FlmBus/copycat/internal/menu"
)

type RofiMenuRenderer struct {
	title   string
	rofiBin string
	args    []string
}

type RofiMenuRendererOpt func(*RofiMenuRenderer)

var _ menu.MenuRenderer = &RofiMenuRenderer{}

func NewRofiMenuRenderer(opts ...RofiMenuRendererOpt) *RofiMenuRenderer {
	title := "Snippet"
	rofiBin := "rofi"
	args := []string{"-dmenu", "-markup", "-markup-rows", "-p", "Snippet", "-i", "-no-custom", "-format", "i"}

	renderer := &RofiMenuRenderer{
		title:   title,
		rofiBin: rofiBin,
		args:    args,
	}

	for _, opt := range opts {
		opt(renderer)
	}

	return renderer
}

func WithTitle(title string) RofiMenuRendererOpt {
	return func(renderer *RofiMenuRenderer) {
		renderer.title = title
	}
}

func WithRofiBin(rofiBin string) RofiMenuRendererOpt {
	return func(renderer *RofiMenuRenderer) {
		renderer.rofiBin = rofiBin
	}
}

func WithArg(arg string) RofiMenuRendererOpt {
	return func(renderer *RofiMenuRenderer) {
		renderer.args = append(renderer.args, arg)
	}
}

func WithTheme(theme string) RofiMenuRendererOpt {
	return func(renderer *RofiMenuRenderer) {
		renderer.args = append(renderer.args, "-theme", theme)
	}
}

func (rofi *RofiMenuRenderer) Render(snippetMenu menu.Menu) (menu.ListItem, error) {
	cmd := exec.Command(rofi.rofiBin, rofi.args...)

	snippetGroups := snippetMenu.GetSnippetGroups()

	listItems := make([]menu.ListItem, 0)

	sb := strings.Builder{}
	for _, group := range snippetGroups {
		titleItem := TitleItem{Icon: group.Icon, Name: group.Name}
		for _, snippet := range group.Snippets {
			snippetItem := SnippetItem{Snippet: snippet}
			sb.WriteString(titleItem.String())
			sb.WriteString(" ")
			sb.WriteString(snippetItem.String())
			sb.WriteString("\n")
			listItems = append(listItems, &snippetItem)
		}
	}

	cmd.Stdin = bytes.NewBufferString(sb.String())

	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out

	err := cmd.Run()
	if err != nil {
		return nil, err
	}

	parsedInt, err := strconv.Atoi(strings.TrimSpace(out.String()))
	if err != nil {
		fmt.Printf("Error parsing index: %v", err)
		return nil, err
	}

	if parsedInt >= 0 || parsedInt < len(listItems) {
		return listItems[parsedInt], nil
	}

	return nil, nil
}
