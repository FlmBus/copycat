package rofi

import (
	"fmt"
	"strings"

	"gitlab.com/FlmBus/copycat/internal/menu"
	"gitlab.com/FlmBus/copycat/internal/snippets"
)

type SnippetItem struct {
	snippets.Snippet
}

var _ menu.ListItem = &SnippetItem{}
var _ menu.SnippetItem = &SnippetItem{}

func (item *SnippetItem) String() string {
	return fmt.Sprintf(
		"<span weight='bold'>%s</span>\t<span font-size='small' style='oblique'>%32s</span>",
		fixedLength(item.Name, 34),
		strings.Join(item.Keywords, ","),
	)
}

func (item *SnippetItem) Template() string {
	return item.Snippet.Template
}

func fixedLength(s string, n int) string {
	if len(s) > n {
		sb := strings.Builder{}
		sb.WriteString(s[:n-3])
		sb.WriteString("...")
		return sb.String()
	}

	tpl := fmt.Sprintf("%%-%ds", n)
	return fmt.Sprintf(tpl, s)
}
