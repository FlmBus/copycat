package menu

type ListItem interface {
	String() string
}

type SnippetItem interface {
	Template() string
}
