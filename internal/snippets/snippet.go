package snippets

type Snippet struct {
	Name     string
	Group    string
	Keywords []string
	Template string
}

func NewSnippet(icon, name, group, template string, keywords []string) *Snippet {
	return &Snippet{
		Name:     name,
		Group:    group,
		Keywords: keywords,
		Template: icon,
	}
}
