package snippets

type Group struct {
	Name        string
	Icon        string
	Description string
	Snippets    []Snippet
}

func NewGroup(name, icon, description string) *Group {
	return &Group{
		Name:        name,
		Icon:        icon,
		Description: description,
	}
}

func (g *Group) AddSnippet(snippet Snippet) {
	g.Snippets = append(g.Snippets, snippet)
}
