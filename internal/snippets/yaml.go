package snippets

import (
	"os"

	"github.com/goccy/go-yaml"
)

type YamlSnippetsFile struct {
	Groups   map[string]YamlGroupsItem   `yaml:"groups"`
	Snippets map[string]YamlSnippetsItem `yaml:"snippets"`
}

type YamlGroupsItem struct {
	Description string `yaml:"description"`
	Icon        string `yaml:"icon"`
}

type YamlSnippetsItem struct {
	Group    string   `yaml:"group"`
	Template string   `yaml:"template"`
	Keywords []string `yaml:"keywords"`
}

func ReadYaml(path string) ([]Group, error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var yamlSnippets YamlSnippetsFile

	err = yaml.Unmarshal(content, &yamlSnippets)
	if err != nil {
		return nil, err
	}

	// Aggregate groups
	groupsMap := make(map[string]*Group)
	for groupName, group := range yamlSnippets.Groups {
		groupsMap[groupName] = &Group{
			Name:        groupName,
			Icon:        group.Icon,
			Description: group.Description,
		}
	}

	// Aggregate snippets
	for snippetName, snippet := range yamlSnippets.Snippets {
		group, ok := groupsMap[snippet.Group]
		if !ok {
			group = &Group{Name: snippet.Group}
			groupsMap[snippet.Group] = group
		}

		group.Snippets = append(group.Snippets, Snippet{
			Name:     snippetName,
			Group:    snippet.Group,
			Keywords: snippet.Keywords,
			Template: snippet.Template,
		})
	}

	groups := make([]Group, 0, len(groupsMap))
	for _, group := range groupsMap {
		groups = append(groups, *group)
	}

	return groups, nil
}
