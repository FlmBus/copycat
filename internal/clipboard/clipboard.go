package clipboard

import (
	"fmt"
	"os/exec"
	"time"

	c "github.com/tiagomelo/go-clipboard/clipboard"
)

func InsertText(text string, delay time.Duration) error {
	clip := c.New()

	if err := clip.CopyText(text); err != nil {
		fmt.Printf("error: %#v\n", err)
	}

	timer := time.NewTimer(delay)
	<-timer.C

	cmd := exec.Command("xdotool", "key", "Ctrl+Shift+V")
	err := cmd.Run()
	return err
}
