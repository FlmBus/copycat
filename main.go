package main

import (
	"fmt"
	"os"
	"path"
	"time"

	_ "embed"

	flag "github.com/spf13/pflag"
	"gitlab.com/FlmBus/copycat/internal/clipboard"
	"gitlab.com/FlmBus/copycat/internal/menu"
	"gitlab.com/FlmBus/copycat/internal/menu/rofi"
	"gitlab.com/FlmBus/copycat/internal/snippets"
)

type Config struct {
	Theme        string
	SnippetFiles []string
	Groups       []string
	PasteDelayMs int
}

var conf *Config

//go:embed snippets.example.yaml
var exampleSnipFile []byte

func init() {
	var theme string = ""
	flag.StringVar(&theme, "theme", "", "rofi theme")

	homeDir, err := os.UserHomeDir()
	if err != nil {
		fmt.Printf("Error getting home directory: %v\n", err)
		os.Exit(1)
	}
	var snippetFiles []string = []string{
		fmt.Sprintf("%s/.copycat.yml", homeDir),
		fmt.Sprintf("%s/.copycat.yml", homeDir),
		fmt.Sprintf("%s/.config/copycat/snippets.yml", homeDir),
		fmt.Sprintf("%s/.config/copycat/snippets.yaml", homeDir),
		fmt.Sprintf("%s/.copycat/snippets.yml", homeDir),
		fmt.Sprintf("%s/.copycat/snippets.yaml", homeDir),
		"/etc/copycat/snippets.yml",
		"/etc/copycat/snippets.yaml",
	}
	var additionalSnippets []string
	flag.StringSliceVar(&additionalSnippets, "snippets", []string{}, "snippets file")

	var groups []string
	flag.StringSliceVar(&groups, "group", []string{}, "group filter")

	var pasteDelayMs int
	flag.IntVar(&pasteDelayMs, "delay", 25, "delay in ms")

	flag.Parse()

	conf = &Config{
		Theme:        theme,
		SnippetFiles: append(snippetFiles, additionalSnippets...),
		Groups:       groups,
		PasteDelayMs: pasteDelayMs,
	}
}

func main() {
	// Read snippets from files
	var fetchedSnipGroups []snippets.Group
	foundFile := false
	for _, snippetFile := range conf.SnippetFiles {
		groups, err := snippets.ReadYaml(snippetFile)
		if err == nil {
			fetchedSnipGroups = append(fetchedSnipGroups, groups...)
			foundFile = true
		}
	}

	if !foundFile {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			fmt.Printf("Error getting home directory: %v\n", err)
			os.Exit(1)
		}

		targetPath := fmt.Sprintf("%s/.config/copycat/snippets.yaml", homeDir)

		if _, err := os.Stat(path.Dir(targetPath)); os.IsNotExist(err) {
			err := os.MkdirAll(path.Dir(targetPath), os.ModePerm)
			if err != nil {
				fmt.Printf("Error creating directory: %v\n", err)
				os.Exit(1)
			}
		}

		err = os.WriteFile(targetPath, exampleSnipFile, 0600)
		if err != nil {
			fmt.Printf("Error creating example file: %v\n", err)
			os.Exit(1)
		}

		fmt.Print("No snippets file found. An example file has been created at ~/.config/copycat/snippets.yaml\n")
		os.Exit(2)
	}

	// Create snippet menu
	snippetMenu := &menu.SnippetMenu{}
	for _, group := range fetchedSnipGroups {
		snippetMenu.AddGroup(&group)
	}

	// Create rofi renderer
	rendererOpts := []rofi.RofiMenuRendererOpt{rofi.WithRofiBin("rofi"), rofi.WithTitle("Snippets")}
	if conf.Theme != "" {
		rendererOpts = append(rendererOpts, rofi.WithTheme(conf.Theme))
	}
	renderer := rofi.NewRofiMenuRenderer(rendererOpts...)

	// Render menu
	for {
		selected, err := renderer.Render(snippetMenu)
		if err != nil {
			fmt.Printf("Error: %v\n", err)
			break
		}

		if selected != nil {
			if snippetItem, ok := selected.(menu.SnippetItem); ok {
				handleSelection(snippetItem)
				return
			}
		}
	}
}

func handleSelection(snippet menu.SnippetItem) {
	delay := time.Duration(conf.PasteDelayMs) * time.Millisecond
	err := clipboard.InsertText(snippet.Template(), delay)
	if err != nil {
		fmt.Printf("Error pasting text: %v\n", err)
	}
}
